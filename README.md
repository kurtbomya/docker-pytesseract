# Docker Image Pytesseract

### Summary

Make available a docker image that can be used as a GitLab runner to execute pytesseract OCR application.

Include this in your `.gitlab-ci.yml` file:

```yml
image: rakaim/pytesseract:latest
```